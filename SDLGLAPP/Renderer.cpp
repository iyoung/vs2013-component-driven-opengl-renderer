#include "Renderer.h"
#include <exception>

Renderer::Renderer()
{
	mWindow = nullptr;
	minorVersion = 0;
	majorVersion = 3;
	MSAAlevel = 1;
	mWindowName = "Window";
}
void Renderer::setConfiguration(RConfig &config)
{
	setMSAALevel(config.MSAA);
	setVerticalSync(config.VSync);
	setMinorVersion(config.mversion);
	setMajorVersion(config.MVersion);
	init(config.WWidth, config.WHeight);
	printf("MSAA: %d\n",config.MSAA);
	printf("Vsync enabled: %d\n",config.VSync);
	printf("Window Size: %d x %d\n",config.WWidth, config.WHeight);

}
RenderError Renderer::init(const int &windowWidth, const int &windowHeight)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
		return SDL_VIDEO_ERROR;

	// Request an OpenGL 3.3 context.
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, majorVersion);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minorVersion);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, MSAAlevel); // Turn on x4 multisampling anti-aliasing (MSAA)
	// Create 800x600 window
	mWindow = SDL_CreateWindow(mWindowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
								windowWidth, windowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (!mWindow) // Check window was created OK
		return SDL_WINDOW_ERROR;

	mContext = SDL_GL_CreateContext(mWindow); // Create opengl context and attach to window
	SDL_GL_SetSwapInterval(vSyncEnabled); // set swap buffers to sync with monitor's vertical refresh rate
	//this is needed for windows 
	glewExperimental = GL_TRUE;
	//initialise glew
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		return GLEW_ERROR;
	}
	//output version
	printf((const char*)glGetString(GL_VERSION));
	printf("\n");

	//enable standard opengl rendering features
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_CULL_FACE);

	imgLoader = std::unique_ptr<SDLImageLoader>(new SDLImageLoader());
	imgLoader->init();
	return NO_ERROR;
}
void Renderer::renderScene()
{
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//draw calls go here
	SDL_GL_SwapWindow(mWindow); // swap buffers
}
RenderError Renderer::UploadImage(SDL_Surface* image, GLuint &textureHandle)
{
	if (!image)
	{
		throw "no image loaded";
	}
	GLuint w = image->w;
	GLuint h = image->h;
	GLuint colours = image->format->BytesPerPixel;

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (image->format->Rmask == 0x000000ff)
			format = GL_RGBA;
		else
			format = GL_BGRA;
	}
	else {             // no alpha
		if (image->format->Rmask == 0x000000ff)
			format = GL_RGB;
		else
			format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	//GLuint texture;
	if (textureHandle == NULL)
		glGenTextures(1, &textureHandle);

	glBindTexture(GL_TEXTURE_2D, textureHandle);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(	GL_TEXTURE_2D, 
					0, 
					internalFormat, 
					w, 
					h, 
					0,
					format, 
					GL_UNSIGNED_BYTE, 
					image->pixels);
	return NO_ERROR;
}
RenderError Renderer::loadTexture(const std::string &fileName, GLuint &textureHandle)
{
	try
	{
		SDL_Surface* image = imgLoader->loadTexture(fileName);
		RenderError error = UploadImage(image, textureHandle);
		loadedTextures.push_back(textureHandle);
		return error;
	}
	catch (char* str)
	{
		printf("error loading texture: %s\n", str);
		return IMGLOADER_ERROR;
	}
	return NO_ERROR;

}
void Renderer::freeTexture(GLuint &textureHandle)
{
	glDeleteTextures(1, &textureHandle);
}
void Renderer::freeAlltextures(std::vector<GLuint> &textures)
{
	glDeleteTextures(textures.size(), textures.data());
}
void Renderer::resizeWindow(const int &windowWidth, const int &windowHeight)
{

}
GLuint Renderer::addEntity(std::shared_ptr<Entity> entity)
{
	return 0;
}
bool Renderer::removeEntity(std::shared_ptr<Entity> entity)
{
	return true;
}
void Renderer::shutDown()
{
	if (!loadedTextures.empty())
	{
		freeAlltextures(loadedTextures);
		loadedTextures.clear();
		printf("Textures unloaded.\n");
	}
	imgLoader->shutDown();
	SDL_GL_DeleteContext(mContext);
	SDL_DestroyWindow(mWindow);
	printf("Renderer shutdown complete.\n");
}
Renderer::~Renderer()
{
}
