﻿#include "MeshLoader.h"


//MeshLoader Definitions
MeshLoader::MeshLoader()
{
}
void MeshLoader::init(const int &numMeshes)
{
	meshes.reserve(numMeshes);
	printf("mesh loader initialization complete.\n");
}
std::shared_ptr<MeshNode> MeshLoader::loadMesh(const std::string &meshFile)
{
	std::shared_ptr<MeshNode> mesh = std::make_shared < MeshNode > ();
	mesh->loadModel(meshFile);
	meshes.push_back(mesh);
	return mesh;
}

MeshLoader::~MeshLoader()
{
	meshes.clear();
}

//MeshNode definitions
MeshNode::MeshNode(void)
{

}

void MeshNode::loadModel(std::string p_FileName)
{
	Assimp::Importer v_assimpImporter;	// Handle for importing the model

	const aiScene* v_assimpScene = v_assimpImporter.ReadFile(p_FileName,				// Load the model from file
		aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_SortByPType);	// and set post-processing flags

	if (!v_assimpScene)	// Check if loading was successful
	{
		std::string error = v_assimpImporter.GetErrorString();
		throw  std::exception((p_FileName + " model failed to load (" + error + ").").c_str());
	}

	loadFromScene(v_assimpScene);	// Start model loading from the Assimp scene structure
	printf("%s loaded.\n",p_FileName.c_str());
	//and finally generate tangents
	//generateTangents(v_Faces,temp_uvs,temp_normals);}
}
void MeshNode::loadQuad(int p_numTiles)
{
	std::string numTiles = "20";//Utils::toString(p_numTiles);
	std::string quadString = "\n\
							 v 1.000000 0.000001 -1.000000\n\
							 v -1.000000 0.000001 -1.000000\n\
							 v -1.000000 -0.000001 1.000000\n\
							 v 1.000000 -0.000001 1.000000\n\
							 vt " + numTiles + ".000000 " + numTiles + ".000000\n\
							 vt 0.000000 " + numTiles + ".000000\n\
							 vt 0.000000 0.000000\n\
							 vt " + numTiles + ".000000 0.000000\n\
							 vn 0.000000 1.000000 0.000000\n\
							 s off\n\
							 f 1/1/1 2/2/1 3/3/1\n\
							 f 4/4/1 1/1/1 3/3/1\n\
							 ";
	/*std::string quadString = "\n\
	v 1.000000 0.000001 -1.000000\n\
	v -1.000000 0.000001 -1.000000\n\
	v -1.000000 -0.000001 1.000000\n\
	v 1.000000 -0.000001 1.000000\n\
	vt 1.500000 1.500000\n\
	vt 0.500000 1.500000\n\
	vt 0.500000 0.500000\n\
	vt 1.500000 0.500000\n\
	vn -0.000000 1.000000 0.000001\n\
	s off\n\
	f 1/1/1 2/2/1 3/3/1\n\
	f 4/4/1 1/1/1 3/3/1\n\
	";*/

	Assimp::Importer v_assimpImporter;	// Handle for importing the model

	const aiScene* v_assimpScene = v_assimpImporter.ReadFileFromMemory(quadString.c_str(), quadString.size(),
		aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_SortByPType | aiProcess_GenUVCoords);

	if (!v_assimpScene)	// Check if loading was successful
	{
		std::string errorString = "Quad model failed to load (";
		throw new std::exception((errorString + v_assimpImporter.GetErrorString() + ").").c_str());
	}

	loadFromScene(v_assimpScene);	// Start model loading from the Assimp scene structure

	//and finally generate tangents
	//generateTangents(v_Faces,temp_uvs,temp_normals);
}
void MeshNode::loadFromScene(const aiScene *p_assimpScene)
{
	// Reserve space in the vectors for every mesh
	m_MeshPool.resize(p_assimpScene->mNumMeshes);

	unsigned int v_numVerticesTotal = 0;
	unsigned int v_numIndicesTotal = 0;

	// Count the number of vertices and indices
	for (unsigned int i = 0; i < p_assimpScene->mNumMeshes; i++)					// For each mesh in the model
	{
		m_MeshPool[i].m_MaterialIndex = p_assimpScene->mMeshes[i]->mMaterialIndex;	// Get index for a material
		m_MeshPool[i].m_NumIndices = p_assimpScene->mMeshes[i]->mNumFaces * 3;		// Calculate the number of indices in the mesh

		m_MeshPool[i].m_BaseVertex = v_numVerticesTotal;	// Get base vertex and index, wich work similar as offset, when drawing from buffers
		m_MeshPool[i].m_BaseIndex = v_numIndicesTotal;

		v_numVerticesTotal += p_assimpScene->mMeshes[i]->mNumVertices;	// Calculate the total number of vertices
		v_numIndicesTotal += m_MeshPool[i].m_NumIndices;				// and indices
	}

	m_NumVerts = v_numIndicesTotal;

	// Reserve space in the vectors, to slighly improve performance of '.push_back's
	m_Vertices.reserve(v_numVerticesTotal);
	m_Normals.reserve(v_numVerticesTotal);
	m_Tangents.reserve(v_numVerticesTotal);
	m_Bitangents.reserve(v_numVerticesTotal);
	m_TextureCoordinates.reserve(v_numVerticesTotal);
	m_Indices.reserve(v_numIndicesTotal);

	// Deal with each mesh
	for (unsigned int i = 0; i < p_assimpScene->mNumMeshes; i++)
		loadFromMesh(p_assimpScene->mMeshes[i]);
}
void MeshNode::loadFromMesh(const aiMesh* p_assimpMesh)
{
	// Make sure that the texture coordinates array exist (by checking if the first member of the array does)
	bool v_textureCoordsExist = p_assimpMesh->mTextureCoords[0] ? true : false;

	// Put the vertex positions, normals, tangents, bitangents, texture coordinate data from Assimp data sets to our data sets (vectors)
	for (unsigned int i = 0; i < p_assimpMesh->mNumVertices; i++)
	{
		m_Vertices.push_back(glm::vec3(p_assimpMesh->mVertices[i].x, p_assimpMesh->mVertices[i].y, p_assimpMesh->mVertices[i].z));
		if (p_assimpMesh->mNormals != NULL)
			m_Normals.push_back(glm::vec3(p_assimpMesh->mNormals[i].x, p_assimpMesh->mNormals[i].y, p_assimpMesh->mNormals[i].z));
		if (p_assimpMesh->mTangents != NULL)
			m_Tangents.push_back(glm::vec3(p_assimpMesh->mTangents[i].x, p_assimpMesh->mTangents[i].y, p_assimpMesh->mTangents[i].z));
		if (p_assimpMesh->mBitangents != NULL)
			m_Bitangents.push_back(glm::vec3(p_assimpMesh->mBitangents[i].x, p_assimpMesh->mBitangents[i].y, p_assimpMesh->mBitangents[i].z));
		if (v_textureCoordsExist)	// Proceed with texture coordinates only if they exsist
			m_TextureCoordinates.push_back(glm::vec2(p_assimpMesh->mTextureCoords[0][i].x, p_assimpMesh->mTextureCoords[0][i].y));
	}

	// Populate the indices (from Assimp to our vectors)
	for (unsigned int i = 0; i < p_assimpMesh->mNumFaces; i++)
		if (p_assimpMesh->mFaces[i].mNumIndices == 3)
		{
			m_Indices.push_back(p_assimpMesh->mFaces[i].mIndices[0]);
			m_Indices.push_back(p_assimpMesh->mFaces[i].mIndices[1]);
			m_Indices.push_back(p_assimpMesh->mFaces[i].mIndices[2]);
		}
}

void MeshNode::clearModelData(void)
{
	m_Vertices.clear();
	m_TextureCoordinates.clear();
	m_Normals.clear();
	m_Tangents.clear();
	m_Bitangents.clear();
	m_Indices.clear();
}
MeshNode::~MeshNode(void)
{
	clearModelData();
}