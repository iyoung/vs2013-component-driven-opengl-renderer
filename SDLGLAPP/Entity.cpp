#include "Entity.h"


Entity::Entity()
{
	isAlive = false;
	isInUse = false;
	position = glm::vec3(0.0f);
	pitch = 0.0f;
	yaw = 0.0f;
	roll = 0.0f;
	meshHandle = 0;
	diffuseHandle = 0;
	numVertices = 0;
	transform = glm::mat4(1.0f);
	uniqueID = 0;
}
void Entity::destroy()
{
	isAlive = false;
	isInUse = false;
	position = glm::vec3(0.0f);
	pitch = 0.0f;
	yaw = 0.0f;
	roll = 0.0f;
	meshHandle = 0;
	diffuseHandle = 0;
	numVertices = 0;
	transform = glm::mat4(1.0f);
}

Entity::~Entity()
{
}
