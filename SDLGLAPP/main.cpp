#include "SDL.h"
// glew extension wrangler
#include "GL\glew.h"
// iostream for console output
#include <iostream>
//GL templated math library, compatible with opengl
#include <glm\glm.hpp>
//SDL image library for loading textures
#include <SDL_image.h>
//SDL true type font library for loading fonts to create onscreen text 
#include <SDL_ttf.h>
//SDL sound library, for playing music etc, if it's needed
#include <SDL_mixer.h>
//program needs time to perform integration
#include <time.h>
//new renderer class
#include "Renderer.h"
#include "EntityManager.h"
#include "ConfigurationLoader.h"
#include "MeshLoader.h"
//simple error function, can be tied into exception handling
// NO GLOBALS. EVER!!! this will need refactored asap
std::unique_ptr<Renderer> renderer;
std::unique_ptr<EntityManager> eManager;
std::unique_ptr<ConfigurationLoader> configloader;
std::unique_ptr<MeshLoader> mloader;
unsigned int numEntities = 10;

void exitFatalError(const char *message) {
	std::cout << message << " ";
	std::cin.get();
	exit(1);
}
void init()
{
	configloader = std::unique_ptr<ConfigurationLoader>(new ConfigurationLoader());
	RConfig config = configloader->loadRendererConfig("config.txt");
	//create & init app objects here
	renderer = std::unique_ptr<Renderer>(new Renderer());
	renderer->setConfiguration(config);
	mloader = std::unique_ptr<MeshLoader>(new MeshLoader());
	mloader->init(10);
	std::shared_ptr<MeshNode> mesh;
	try
	{
		mesh = mloader->loadMesh("Cube.obj");
	}
	catch (std::exception e)
	{
		printf("error: %s\n", e.what());
	}
	printf("renderer initialized\n");
	//create and init an Entity Manager for testing
	eManager = std::unique_ptr<EntityManager>(new EntityManager());
	EntityManagerError err = eManager->init(numEntities);
	if (err != EM_NO_ERROR)
	{
		exitFatalError("entity manager initialization failure");
	}
	printf("entity manager initialized\n");
	GLuint texID = 0;
	if (renderer->loadTexture("Stone_JFdwarvenFloor_512_d.tga", texID) != NO_ERROR)
	{
		exitFatalError("texture load failure\n");
	}
}
void shutDown()
{
	renderer->shutDown();
	eManager->shutDown();
}
bool handleEvent(SDL_Event &event)
{
	//event handling stuff goes here
	//if exit condition met
	//return false
	//else
	if (event.type == SDL_QUIT)
		return false;
	else
		return true;
}

void update(float deltaTime)
{
	//any scene updates go here
}

int main(int argc, char** argv)
{
	bool running = true;	
	float deltaTime = 0.0f;
	float lastTime = clock();
	float currentTimer = lastTime;

	SDL_Event sdlEvent;  // variable to detect SDL events
	init();
	while (running)
	{
		while (SDL_PollEvent(&sdlEvent)) {
				running = handleEvent(sdlEvent);
		}
		update(deltaTime / 1000.0f);
		renderer->renderScene();

		lastTime = currentTimer;
		currentTimer = clock();
		deltaTime = currentTimer - lastTime;
	}
	shutDown();
	SDL_Quit();
	std::cin.get();
	return 0;
}