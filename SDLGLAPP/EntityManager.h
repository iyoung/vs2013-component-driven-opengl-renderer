#pragma once
#include "Entity.h"
#include <string>
#include <memory>
#include <vector>
enum EntityManagerError
{
	EM_NO_ERROR,
	EM_CANNOT_CREATE,
	EM_CANNOT_FIND,
	EM_ZERO_CAP
};
class EntityManager
{
public:
	EntityManager();
	EntityManagerError init(const unsigned int &maxEntities);
	void shutDown();
	std::shared_ptr<Entity> getEntity();
	EntityManagerError unloadEntity(std::shared_ptr<Entity> pEntity);
	~EntityManager();
private:
	std::vector< std::shared_ptr< Entity > > mEntities;
};

