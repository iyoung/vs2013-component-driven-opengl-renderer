#pragma once
#include "Renderer.h"
class ConfigurationLoader
{
public:
	ConfigurationLoader();
	RConfig loadRendererConfig(const std::string &configFile);
	void saveRenderingConfig(const std::string &targetFileName);
	~ConfigurationLoader();

};

