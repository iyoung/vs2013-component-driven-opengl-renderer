#pragma once
#include <glm\glm.hpp>
#include <GL\glew.h>
#include <string>
#include <memory>
#include <vector>

class Entity
{
public:
	friend class Renderer;
	friend class EntityManager;
	Entity();
	//mutator functions
	void setName(const std::string &Ename)			{	mName		= Ename	;		}
	void setID(const GLuint &ID)					{	uniqueID	= ID	;		}
	//position mutators
	void setPosition(const glm::vec3 &newPosition)	{	position	= newPosition;	}
	void setMove(const glm::vec3 &offset)			{	position	+= offset;		}

	void setOrientation(const float &newYaw, const float &newPitch, const float &newRoll){
		pitch = newPitch;
		yaw = newYaw;
		roll = newRoll;
	}
	//orientation mutators
	void setYaw(const float &newYaw)				{	yaw	= newYaw		;		}
	void setPitch(const float &newPitch)			{	pitch = newPitch	;		}
	void setRoll(const float &newRoll)				{	roll	= newRoll	;		}

	void rotateYaw(const float &deltaYaw)			{	yaw	+= deltaYaw		;		}
	void rotatePitch(const float &deltaPitch)		{	pitch += deltaPitch	;		}
	void rotateRoll(const float &deltaRoll)			{	roll	+= deltaRoll;		}

	void setRotate(const float &deltaYaw, const float &deltaPitch, const float &deltaRoll){
		pitch += deltaPitch;
		yaw += deltaYaw;
		roll += deltaRoll;
	}
	//entity state mutators
	void kill()								{	isAlive	= false	;	}
	void revive()							{	isAlive	= true	;	}
	void activate()							{	isInUse	= true	;	}
	//Entity accessor functions
	const std::string &getName()	const	{	return mName	;	}
	const GLuint getID()			const	{	return uniqueID	;	}
	const bool &Alive()				const	{	return isAlive	;	}
	const bool &InUse()				const	{	return isInUse	;	}
	//orientation position accessors
	const float &getYawAngle()		const	{	return yaw		;	}
	const float &getPitchAngle()	const	{	return pitch	;	}
	const float &getRollAngle()		const	{	return roll		;	}
	const glm::vec3 &getPosition()	const	{	return position	;	}

	//friend functions for easy comparisons and to enable use with standard library
	const bool operator == (Entity &other) const 
	{
		return (uniqueID == other.getID());
	}
	~Entity();
private:
	void destroy();
	std::vector<std::string> resources;
	std::string mName;
	GLuint uniqueID;
	glm::vec3 position;
	//floats representing each axis of orientation
	float	pitch,
			yaw,
			roll;
	//transform for rendering purposes
	glm::mat4 transform;
	//basic graphics data: 3d model & textures, plus number of vertices in model
	GLuint	meshHandle,
			diffuseHandle,
			numVertices;
	GLuint *VBOset;
	//entity variables, indicates if entity is to be unloaded, or reused, or is in use currently
	bool	isAlive,
			isInUse;
};

