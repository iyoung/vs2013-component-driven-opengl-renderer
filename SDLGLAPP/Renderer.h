#pragma once
#include "SDL.h"
// glew extension wrangler
#include "GL\glew.h"
// iostream for console output
#include <iostream>
//GL templated math library, compatible with opengl
#include <glm\glm.hpp>
//SDL image library for loading textures
#include <SDL_image.h>
//SDL true type font library for loading fonts to create onscreen text 
#include <SDL_ttf.h>
//as this is an entity driven rendering engine,
#include "Entity.h"
#include "SDLImageLoader.h"
//initialization will return one of these enums
enum RenderError
{
	NO_ERROR,
	SDL_VIDEO_ERROR,
	SDL_WINDOW_ERROR,
	IMGLOADER_ERROR,
	TEXTURELOAD_ERROR,
	GLEW_ERROR
};

struct RConfig
{
	GLuint	mversion,
			MVersion,
			MSAA,
			VSync,
			WWidth,
			WHeight;
};

class Renderer
{
public:
	Renderer();
	//functions to set some basic settings
	//these should be called first, or you only get a basic 3.0 context with only 1 level of MSAA
	void setConfiguration(RConfig &config);
	void setMSAALevel(const GLuint &level)					{	MSAAlevel		= level			;	}
	void setMinorVersion(const GLuint &mVersion)			{	minorVersion	= mVersion		;	}
	void setMajorVersion(const GLuint &MVersion)			{	majorVersion	= MVersion		;	}
	void setWindowName(const std::string &windowName)		{	mWindowName		= windowName	;	}
	void setVerticalSync(const bool &vSyncSwitch)			{	vSyncEnabled	= vSyncSwitch	;	}
	RenderError loadTexture(const std::string &fileName, GLuint &textureHandle);
	RenderError init(const int &windowWidth, const int &windowHeight);
	//engine functions
	void resizeWindow(const int &windowWidth, const int &windowHeight);
	GLuint addEntity(std::shared_ptr<Entity> entity);
	bool removeEntity(std::shared_ptr<Entity> entity);
	void renderScene();
	void shutDown();
	~Renderer();
private:
	RenderError UploadImage(SDL_Surface* image, GLuint &textureHandle);
	void freeTexture(GLuint &textureHandle);
	void freeAlltextures(std::vector<GLuint> &textures);
	//not a lot going on here, just obvious stuff
	//the real magic is in the client code
	SDL_Window		*mWindow;
	SDL_GLContext	mContext;

	int				mWinHeight, 
					mWinWidth;

	GLuint			minorVersion,
					majorVersion,
					MSAAlevel;

	bool			vSyncEnabled,
					mChanged;

	std::string		mWindowName;
	std::vector<GLuint> loadedTextures;
	std::unique_ptr<SDLImageLoader> imgLoader;
};

