#include "ConfigurationLoader.h"
#include <iostream>
#include <fstream>
#include <sstream>
ConfigurationLoader::ConfigurationLoader()
{
}
RConfig ConfigurationLoader::loadRendererConfig(const std::string &configFile)
{
	
	RConfig config;
	std::string line;
	try
	{
		std::ifstream file(configFile.c_str());
		if (file.is_open())
		{
			while (std::getline(file, line))
			{
				if (line.find("#resolution") != std::string::npos)
				{
					std::istringstream s(line.substr(11));
					s >> config.WWidth >> config.WHeight;
				}
				else if (line.find("#GFXversion") != std::string::npos)
				{
					std::istringstream s(line.substr(11));
					s >> config.MVersion >> config.mversion;
				}
				else if (line.find("#Vsync") != std::string::npos)
				{
					std::istringstream s(line.substr(6));
					s >> config.VSync;
				}
				else if (line.find("#MSAA") != std::string::npos)
				{
					std::istringstream s(line.substr(5));
					s >> config.MSAA;
				}
				else
					continue;
			}
		}
	}
	catch (std::ifstream::failure e)
	{
		printf("config not loaded: %s\n",e.what());
	}
	printf("config: %s loaded.\n", configFile.c_str());
	return config;
}

ConfigurationLoader::~ConfigurationLoader()
{
}
