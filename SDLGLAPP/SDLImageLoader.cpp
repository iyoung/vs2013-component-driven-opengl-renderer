#include "SDLImageLoader.h"
#include <exception>

SDLImageLoader::SDLImageLoader()
{
}
void SDLImageLoader::init()
{
	int flags = IMG_INIT_JPG | IMG_INIT_PNG;
	int init = IMG_Init(flags);
	if (init&init != flags)
	{
		printf("failed to load jpg/png support");
		printf("IMG_Init: %s\n", IMG_GetError());
	}
	if (TTF_Init() < 0)
	{
		printf("TTF_Init failed");
	}
	printf("IMG loader initialized\n");
}
void SDLImageLoader::shutDown()
{
	//shut down SDL_image & TTF extensions
	IMG_Quit;
	TTF_Quit;
	printf("IMG loader shutdown complete\n");
}
void SDLImageLoader::freeImage(SDL_Surface* image)
{
	//free up ram used by image
	if (image != nullptr)
	{
		SDL_FreeSurface(image);
	}
}
SDL_Surface* SDLImageLoader::loadTexture(const std::string &fname)
{
	//create an image and initialise it with the filename
	//if it loaded ok, return it, else out put message to console
	SDL_Surface* image = IMG_Load(fname.c_str());
	if (!image)
	{
		printf("IMG_Load: %s\n", IMG_GetError());
		return nullptr;
		throw "image load fail\n";
	}
	printf("image: %s loaded\n", fname.c_str());
	return image;
}
SDL_Surface	*SDLImageLoader::textToTexture(const std::string &textString,
	const float &red,
	const float &green,
	const float &blue,
	const float &alpha,
	TTF_Font* font)
{
	SDL_Color colour = { red, green, blue, alpha };
	SDL_Color background = {0,0,0};

	SDL_Surface *image = TTF_RenderText_Blended(font, textString.c_str(), colour);
	if (!image)
	{
		printf("text image not created\n");
		return nullptr;
	}
	return image;
}
TTF_Font* SDLImageLoader::loadFont(const std::string &fname, const int &fontSize)
{
	//create a font and initialise it with the filename & font size
	//if it loaded ok, return it, else out put message to console
	TTF_Font* font = TTF_OpenFont(fname.c_str(), fontSize);
	if (!font)
	{
		printf("font failed to load");
		return nullptr;
	}
	printf("font: %s loaded\n",fname.c_str());

	return font;
}
SDLImageLoader::~SDLImageLoader()
{
}
