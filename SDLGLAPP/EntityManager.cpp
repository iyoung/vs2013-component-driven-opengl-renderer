#include "EntityManager.h"
#include <algorithm>
#include <exception>
EntityManager::EntityManager()
{
}
EntityManagerError EntityManager::init(const unsigned int &maxEntities)
{
	if (maxEntities > 0)
	{
		try
		{
			mEntities.reserve(maxEntities);
		}
		catch (std::exception e)
		{
			mEntities.clear();
			return EM_CANNOT_CREATE;
		}
		return EM_NO_ERROR;
	}
	else
		return EM_ZERO_CAP;
}
std::shared_ptr<Entity> EntityManager::getEntity()
{
	for (std::size_t i = 0; i < mEntities.size(); ++i)
	{
		if (mEntities[i]==nullptr)
		{
			mEntities[i] = std::make_shared<Entity>();
		}
		if (!mEntities[i]->Alive() && !mEntities[i]->InUse())
		{
			mEntities[i]->revive();
			mEntities[i]->setID((GLuint)i);
			mEntities[i]->activate();
			return mEntities[i];
		}
	}
	throw "Entity capacity reached. cannot create";
}
EntityManagerError EntityManager::unloadEntity(std::shared_ptr<Entity> pEntity)
{
	if (mEntities[pEntity->getID()] != nullptr)
	{
		if (mEntities[pEntity->getID()] == pEntity)
		{
			mEntities[pEntity->getID()]->destroy();
			return EM_NO_ERROR;
		}
		else
			return EM_CANNOT_FIND;
	}
	return EM_CANNOT_FIND;
}
void EntityManager::shutDown()
{
	mEntities.clear();
	printf("Entity manager shutdown complete\n");
}
EntityManager::~EntityManager()
{

}
