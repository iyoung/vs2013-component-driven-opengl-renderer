#pragma once
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <string>
class SDLImageLoader
{
public:
	SDLImageLoader();
	void init();
	SDL_Surface* loadTexture(const std::string &fname);
	SDL_Surface* textToTexture(	const std::string &textString,	
								const float &red, 
								const float &green, 
								const float &blue, 
								const float &alpha,
								TTF_Font* font);
	TTF_Font* loadFont(const std::string &fname, const int &fontSize);
	void freeImage(SDL_Surface* image);
	void shutDown();
	~SDLImageLoader();
};

